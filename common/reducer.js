
let INITIAL_STATE = {
  isLoading: false,
  isError: false,
  error: null


}



const Actionhandlers = {
  ["LOADING"]: (state, action) => {
    return {
      ...state,
      isLoading: true,
    }
  },
  ["LOADING_COMPLETE"]: (state, action) => {
    return {
      ...state,
      isLoading: false,
    }
  },
  ['IS_ERROR']: (state, action) => {
    return {
      ...state,
      isError: true,
      error: action.payload
    }
  },








};
export default (state = INITIAL_STATE, action) => {

  const currenthandler = Actionhandlers[action.type]
  if (currenthandler) {
    return currenthandler(state, action);
  }
  return state;
}


