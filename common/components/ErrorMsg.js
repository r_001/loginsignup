import React, { useState } from 'react'
import { Text, Container } from 'native-base';


import Dialog, { DialogContent, DialogTitle, DialogFooter, DialogButton } from 'react-native-popup-dialog';


export default ErrorMsg = ({ title, msg }) => {
  const [visible, setVisible] = useState(true);

  return (
    <Container >

      <Dialog
        visible={visible}
        width={0.7}
        height={300}
        dialogTitle={<DialogTitle title={title} textStyle={{ fontWeight: '500' }} />}

        footer={

          < DialogFooter bordered={false} >

            <DialogButton
              text="CANCEL"
              bordered

              onPress={() => {
                setVisible(false);
              }}
            />
            <DialogButton
              bordered
              text="OK"

              onPress={() => { }}
            />
          </DialogFooter>
        }
      >
        <DialogContent >


          <Text>{msg}</Text>
        </DialogContent>
      </Dialog>
    </Container >
  )
}








