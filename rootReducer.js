import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import userDetails from './common/userDetails';
import app from './common/reducer';



export default combineReducers({
  form: formReducer,
  userDetails: userDetails,
  app: app


});