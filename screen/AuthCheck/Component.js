import React, { useEffect, useState } from 'react';
import Loader from '../../common/components/Loader';
import { Container, Content, Text } from 'native-base';
import { Image, StyleSheet } from 'react-native';
import * as firebase from 'firebase';
import { Redirect } from 'react-router-native'


const styles = StyleSheet.create({
  Container: {

    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: 300,
    height: 150,


  },


});


export default AuthCheck = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [isLoding, setIsLoding] = useState(true);
  const [notLogin, setNotLogin] = useState(false);
  useEffect(() => {
    firebase.auth().onAuthStateChanged(function (user) {
      if (user) {
        // User is signed in
        setIsLoggedIn(true);
        setIsLoding(false);


      } else {
        setNotLogin(true);
        setIsLoding(false);
      }
    });
  }, []);
  if (isLoggedIn) {
    return <Redirect to="/profile" />
  }
  if (notLogin) {
    return <Redirect to="/login" />
  }


  return (
    <Container style={styles.Container}>
      {isLoding && <Loader />}

      <Image source={require("../../assets/logo/heballes1.jpg")} style={styles.logo} />

    </Container>

  )

}