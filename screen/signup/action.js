import * as firebase from 'firebase';

export const onSubmit = (values, history) => dispatch => {
  console.log(values);
  dispatch({
    type: "LOADING"

  });

  firebase.auth().createUserWithEmailAndPassword(values.email, values.password)
    .then(() => {

      firebase.auth().onAuthStateChanged(user => {
        if (user) {
          // User is signed in
          dispatch({
            type: 'USER_DETAILS',
            payload: user
          })
          dispatch({
            type: 'LOADING_COMPLETE'
          })

        }
      });
    }).catch((error) => {
      // Handle Errors here.
      console.log("error", error.message)
      var errorCode = error.code;
      var errorMessage = error.message;
      dispatch({
        type: 'LOADING_COMPLETE'
      })

      return dispatch({
        type: "IS_ERROR",
        payload: errorMessage
      })
      // ...
    });





}