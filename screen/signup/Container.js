import { connect } from "react-redux";
import Component from './Component';
import { onSubmit } from './action';


const mapStateToProps = (state) => ({
  isLoading: state.app.isLoading,
  isError: state.app.isError,
  errormsg: state.app.error,
})

const mapDispatchToProps = {
  onSubmit,
};

export default connect(mapStateToProps, mapDispatchToProps)(Component);