import * as firebase from 'firebase';
let provider = new firebase.auth.GoogleAuthProvider();

export const onSubmit = (values, history) => (dispatch, getState) => {
  const { userDetails } = getState();
  console.log(userDetails);
  dispatch({
    type: "LOADING"

  });

  firebase.auth().signInWithEmailAndPassword(values.email, values.password)
    .then(() => {

      dispatch({
        type: "LOADING_COMPLETE"

      });

    }).catch((error) => {
      // Handle Errors here.
      console.log("error", error.message)
      var errorCode = error.code;
      var errorMessage = error.message;
      dispatch({
        type: "LOADING_COMPLETE"

      });
      return dispatch({
        type: "IS_ERROR",
        payload: errorMessage
      })
      // ...
    });

}

export const googleLogin = () => dispatch => {

  firebase.auth().signInWithRedirect(provider);
  firebase.auth().getRedirectResult().then(function (result) {
    console.log(result)
    if (result.credential) {
      // This gives you a Google Access Token. You can use it to access the Google API.
      var token = result.credential.accessToken;
      // ...
    }
    // The signed-in user info.
    var user = result.user;
  }).catch(function (error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
    // The email of the user's account used.
    var email = error.email;
    // The firebase.auth.AuthCredential type that was used.
    var credential = error.credential;
    // ...
  });



}