
import { connect } from "react-redux";
import { onSubmit, googleLogin } from './action';
import Component from './Component';

const mapStateToProps = (state) => ({
  isLoading: state.app.isLoading,
  isError: state.app.isError,
  errormsg: state.app.error,

})


const mapDispatchToProps = {
  onSubmit,
  googleLogin
};

export default connect(mapStateToProps, mapDispatchToProps)(Component);