import React, { useEffect } from 'react';
import { StyleSheet, Image, Platform, StatusBar, TouchableOpacity } from 'react-native';
import { Field, reduxForm } from 'redux-form';
import { Link } from "react-router-native";
import { Item, Input, View, Text, Container, Label, Button, Body, Header, Content, StyleProvider } from 'native-base';
import { Ionicons } from '@expo/vector-icons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Loader from '../../common/components/Loader';
import ErrorMsg from '../../common/components/ErrorMsg'
const styles = StyleSheet.create({
  container: {
    flex: 1,
    ...Platform.select({
      android: {
        marginTop: StatusBar.currentHeight
      }
    })

  },

  button: {
    justifyContent: 'center',
    alignItems: 'center',
    color: '#FFFFFF',
    marginBottom: 10,
    marginTop: 10,
  },
  inputBox: {
    marginBottom: 15,
    marginTop: 15,
  },
  logo: {
    width: 300,
    height: 150,




  },


});
const renderInput = ({ input, label, icon, style, type, secureTextEntry, meta: { touched, error, warning } }) => {
  return (
    <>
      <Item floatingLabel style={styles.inputBox}   >
        <Label>{label}</Label>
        <Input
          {...input}
          secureTextEntry={secureTextEntry}
          type={type}
        />
      </Item>
      {touched && error ? <Text  >{error}</Text> : null}
    </>
  )
}

const Login = ({ onSubmit, handleSubmit, isLoading, isError, errormsg, googleLogin }) => {

  return (
    <>

      <Container style={styles.container}>
        {isLoading && <Loader />}
        {isError && <ErrorMsg title='ERROR' msg={errormsg} />}
        <Header style={{ backgroundColor: '#FFFFFF', paddingBottom: 100, paddingTop: 100, paddingLeft: 50 }}>
          <Body>
            <Image source={require("../../assets/logo/heballes1.jpg")} style={styles.logo} />
          </Body>

        </Header>
        <KeyboardAwareScrollView
          enableOnAndroid
          enableAutomaticScroll
          keyboardOpeningTime={0}
          extraHeight={Platform.select({ android: 100 })}
          keyboardShouldPersistTaps={'handled'}
          showsVerticalScrollIndicator={false}
          automaticallyAdjustContentInsets={false}
          scrollEventThrottle={10}
          resetScrollToCoords={{ x: 0, y: 0 }}
        >

          <Content padder>
            <Button primary block style={styles.button}>
              <Ionicons name="logo-facebook" size={32} color="#FFFFFF" />
              <Text >Login With Facebook</Text>
            </Button>
            <Button block style={{
              ...styles.button,
              backgroundColor: "#eb5634",
            }} onPress={googleLogin}>
              <Ionicons name="logo-google" size={32} color="white" />
              <Text  >Login With Google</Text>
            </Button>
            <View style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}>
              <Text > ──────── OR  ────────</Text>
            </View>
            <Field
              name="email"
              component={renderInput}
              label=' Email'

            />
            <Field

              name="password"
              secureTextEntry={true}
              component={renderInput}
              label='Password'
              type="email"

            />


            <Button block style={{
              ...styles.button,
              backgroundColor: '#FF8C00'
            }}
              onPress={handleSubmit(onSubmit)}

            >
              <Text
                light style={{
                  fontWeight: '500',


                }} >Login
              </Text>

            </Button>

            <View style={{
              marginTop: 20
            }}>
              <Link component={TouchableOpacity} to={"/signup"}>
                <Text >Do not have an account yet? Signup
          </Text>
              </Link>
            </View>

          </Content>
        </KeyboardAwareScrollView>

      </Container >




    </>
  )
}


const validate = (values) => {
  const errors = {};

  if (!values.email) {
    errors.email = "Email is required"
  }
  if (!values.password) {
    errors.password = "password is required"
  }

  return errors;
};



export default reduxForm({
  form: "login",
  validate,

})(Login);
