import React from 'react';
import { Provider } from "react-redux";
import { AppLoading } from 'expo';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';
import Login from './screen/login';
import Signup from './screen/signup';
import { PersistGate } from 'redux-persist/integration/react';
import { NativeRouter, Route } from "react-router-native";
import persist from "./createStore";
import Profile from './screen/profile';
import AuthCheck from './screen/AuthCheck/Component';
import * as firebase from 'firebase';
import { firebaseConfig } from './config';
firebase.initializeApp(firebaseConfig);




const persistStore = persist();

export default class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
    };
  }
  async componentDidMount() {
    await Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      ...Ionicons.font,
    });


    this.setState({ isReady: true });
  }

  render() {
    if (!this.state.isReady) {
      return <AppLoading />;
    }


    return (
      <Provider store={persistStore.store} >
        <PersistGate loading={null} persistor={persistStore.persistor}>
          <NativeRouter>
            <Route exact path="/" component={AuthCheck} />
            <Route path="/login" component={Login} />
            <Route path="/signup" component={Signup} />
            <Route path="/profile" component={Profile} />

          </NativeRouter>

        </PersistGate>
      </Provider>
    );
  }
}







